<?php
/**
 * The default template for displaying all pages.
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">

<?php
while ( have_posts() ) : the_post();
?>

	<?php get_template_part( 'loop-templates/content', 'page' ); ?>

<?php
endwhile;
?>

</div><!-- .container -->

<?php
get_footer();
