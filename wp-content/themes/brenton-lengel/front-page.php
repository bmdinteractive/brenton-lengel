<?php
/**
 * The front page template.
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">

<?php
while ( have_posts() ) : the_post();
?>
	<div id="feature-img">
		<?php //echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
	</div>

	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
		<div class="entry-content">

			<?php the_content(); ?>


		</div><!-- .entry-content -->

	</article><!-- #post-## -->

<?php
endwhile;
?>

</div><!-- .container -->

<?php
get_footer();
