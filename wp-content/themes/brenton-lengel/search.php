<?php
/**
 * The template for displaying search results pages.
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="col content-col">

<?php
if ( have_posts() ) :
?>

			<header class="page-header page-header--search">
				<h1 class="site-title"><?php printf(
				/* translators:*/
				 esc_html__( 'Search Results for: %s', 'birdstrap' ),
					'<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<div class="archive-list archive-list--search">
<?php
	while ( have_posts() ) : the_post();
	// Support post formats, otherwise look for custom post type tempalate
		$type = get_post_type();
		if ( $type == 'post' ) {
			$type = get_post_format();
		}
	?>
				<?php get_template_part( 'loop-templates/search', $type ); ?>

<?php
	endwhile;
else :
?>

				<?php get_template_part( 'loop-templates/none' ); ?>

<?php
endif;
?>

			</div><!-- .archive-list -->

			<?php birdstrap_pagination(); ?>

		</div><!-- .content-col -->

		<div class="col-md-<?php sidebar_columns(); ?> <?php sidebar_position(); ?> sidebar-col">

			<?php get_sidebar( 'blog' ); ?>

		</div><!-- .sidebar-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php
get_footer();
