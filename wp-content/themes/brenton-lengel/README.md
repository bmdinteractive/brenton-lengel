
# Birdstrap WordPress Theme Framework

__BLACKBIRD/DIGITAL__

A WordPress starter theme with [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/), [Font Awesome 5](https://fontawesome.com/), [Sass](https://sass-lang.com/), [Gulp](https://gulpjs.com/), [Webpack](https://webpack.js.org/), and [BrowserSync](https://browsersync.io/), with influences from [Underscores (\_s)](https://underscores.me/) and support for [WooCommerce](https://woocommerce.com/).

## Basic Features

- Basic templates using Bootstrap markup
- Comes with Bootstrap (v4) Sass source files which include ways to override colors and other settings
- [Font Awesome 5](https://fontawesome.com/) webfonts
- Webpack allows you to import JS libraries instead of worrying about when to include them as script tags
- Jetpack ready
- WooCommerce template support
- Page Navigation Widget included that provides navigation to/from child pages
- No difference between development file output and built-for-production file output. No more having to remember to do a `gulp build` before committing to the repo!
- Humans.txt included! Remember to edit it!

## Installation

- Clone or download this repository into your WordPress themes folder.
- Remove .git folder and rename the containing folder appropriately for your new theme
- Change theme name in `style.css` and in `package.json`
- Set the `version` number in `package.json` to 0.1.0 and use this number to version your theme

## Development Workflow

### Installing Dependencies

- Make sure you have [Node.js](https://nodejs.org/en/) installed globally
- Open your terminal and `cd` to the location of your Birdstrap copy
- Run: `npm install`
- The install should create a `config.json` file for you that you should edit to suit your local development needs.

### Running and Watching Files

Run the development workflow which will watch your files and compile your Sass/JS and reload your browser with: `gulp` (or `npm start`, which just runs the `gulp` command).

This will also update the theme version number from your `package.json` file, which is also used when enqueueing scripts & styles. So if you need to bust the client cache, increment your version number!

## Building for Production

If you've been developing using the `gulp dev` task, your files are already set for production use! During development CSS and JS files are compiled and minified and sourcemapped in the same way as the `gulp build` task, the only difference being that the dev task integrates with BrowserSync to refresh the browser. This means that there is no need to generate `____.min.js` and `____.min.css` files, and no need to switch them out during development since the sourcemaps handle live inspection for you.

## CSS Source Files

`/src/scss/`

This theme system uses Sass to build CSS, which allows us not only all of the benefits of Sass in our own custom styles, but also allows us to integrate Bootstrap CSS directly. This means we can alter Bootstrap defaults without extensive and messy CSS overrides (see `/src/scss/settings/_bootstrap.scss`).

### Sass File Organization

Sass files are organized by purpose, and are imported into the main file in this order:

* `/settings/*`: For setting variables only—these files should output no CSS unless they are including 3rd party styles.
* `/util/*`: For Sass constructs like functions and mixins—these should also generally output no CSS (with the exception of `%placeholders`).
* `/global/*`: For global styles that are included first and may be overridden in more specific contexts.
* `/components/*`: For reusable parts, generally for individual tag styling.
* `/modules/*`: For larger systems of styles which may contain overridden `component` styles.
* `/templates/*`: For template-level styles, which may contain overridden `module` styles
* `/theme.scss`: The entry point for all of the above files, which is output on all pages of the site
* `/editor.scss`: Styles that should appear in the WordPress WYSIWYG/Gutenberg editor, generally a subset of the files that are imported into `theme.scss`

__Any particular partial file within a particular folder should not rely on variables, mixins, functions, etc from any other partial file within the same folder, as they may be imported in any order. The only reliable dependency order is the folder order outlined above.__

## JavaScript Source Files

`/src/js/`

All JS files in the root of the `/src/js/` folder will be processed by Webpack and output to the main `/js/` folder for enqueuing. Any files in subfolders must be imported into a root file in order for the code to be included. See the `theme.js` file for reference.

__Do not add JS files directly to the `/js/` folder, as they are wiped during the build process.__

## Page Templates

Page templates can be found in the `/page-templates` folder. Three default templates are provided that override the default sidebar layout.

## Loop Templates

Templates for various modular pieces of content meant for output within the WordPress loop can be found in the `/loop-templates` folder. These follow a naming convention which matches with the functionality of `get_template_part( $slug, $name )` where `$slug` is the broad category of the module, and `$name` is the special subtype of the module. Therefore, the specialized loop templates are named `{$slug}-{$name}.php`, and a `{$slug}.php` file __SHOULD__ also be present—*WordPress will default to this file if it cannot find the specialized version*. So, if you need a custom post type to be rendered differently than normal posts, you would create a `content-{$post_type}.php` file, and probably a `excerpt-{$post_type}.php` file if your post type has an archive. The default `index.php` and `archive.php` templates have support for both custom post type templates made with the naming convention above and also for [post formats](https://codex.wordpress.org/Post_Formats).

## Page Navigation Widget

Include the `Page Navigation Widget` in your page sidebar to automatically output a menu with the top-level page and all of its sub-pages in the page hierarchy.

## Font Awesome 5

Font Awesome 5 icons are included by way of traditional webfonts. Other methods of including the icons, including the JS/SVG approach, were not flexible enough for our needs. This means that you could, for example, create a custom field for the user to select any icon without worrying if the icon has been included properly.

In order to somewhat offset the weight of including the entire Font Awesome icon library, we only include the styles required for the site design. Generally, we use the "solid" style, as well as the "brands" font for social network links. Which fonts are included is controlled in the `config.js` file, but you must also manually include them in the `/src/scss/fonts.scss` file.

## To Do

- See https://bitbucket.org/blackbirddev/birdstrap/issues/

## Licenses & Credits

- Font Awesome: https://fontawesome.com/license (Font: SIL OFL 1.1, CSS: MIT License)
- Bootstrap: http://getbootstrap.com | https://github.com/twbs/bootstrap/blob/master/LICENSE (Code licensed under MIT documentation under CC BY 3.0.)
and of course
- jQuery: https://jquery.org | (Code licensed under MIT)
- WP Bootstrap Navwalker by Edward McIntyre: https://github.com/twittem/wp-bootstrap-navwalker | GNU GPL
- Bootstrap Gallery Script based on Roots Sage Gallery: https://github.com/roots/sage/blob/5b9786b8ceecfe717db55666efe5bcf0c9e1801c/lib/gallery.php
