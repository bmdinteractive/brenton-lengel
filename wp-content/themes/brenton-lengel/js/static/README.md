
Static JS
=========

This folder is safe from the `clean` step of the build process. If you need to
add static JS that isn't part of the build workflow, add it here.
