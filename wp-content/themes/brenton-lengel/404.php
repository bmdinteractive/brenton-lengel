<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">

	<header class="page-header page-header--404">

		<h1 class="site-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'birdstrap' ); ?></h1>

	</header><!-- .page-header -->

	<div class="site-content">

		<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?',
		'birdstrap' ); ?></p>

		<?php get_search_form(); ?>

		<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

<?php
if ( birdstrap_categorized_blog() ) : // Only show the widget if site has multiple categories.
?>

		<div class="widget widget_categories">

			<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'birdstrap' ); ?></h2>

			<ul>
				<?php
				wp_list_categories( array(
					'orderby'    => 'count',
					'order'      => 'DESC',
					'show_count' => 1,
					'title_li'   => '',
					'number'     => 10,
				) );
				?>
			</ul>

		</div><!-- .widget -->

<?php
endif;

/* translators: %1$s: smiley */
$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'birdstrap' ), convert_smilies( ':)' ) ) . '</p>';
?>

		<?php the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" ); ?>

		<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

	</div><!-- .site-content -->

</div><!-- .container -->

<?php
get_footer();
