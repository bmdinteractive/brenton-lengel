<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package birdstrap
 */

if ( ! is_active_sidebar( 'page-sidebar' ) ) {
	return;
}
?>

<div class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'page-sidebar' ); ?>

</div><!-- .widget-area -->
