/**
 * File fonts.js
 *
 * Responsible for asynchronously loading webfonts
 */

import WebFont from 'webfontloader';

const webfonts = config.webfonts || {};

// Handle loading Font Awesome
if (webfonts.custom) {
	if (webfonts.custom.families) {
		webfonts.custom.families.push('Font Awesome 5 Pro');
	}
	else {
		webfonts.custom.families = ['Font Awesome 5 Pro'];
	}
	if (webfonts.custom.urls) {
		webfonts.custom.urls.push(`${config.themeurl}/css/fonts.css`);
	}
	else {
		webfonts.custom.urls = [`${config.themeurl}/css/fonts.css`];
	}
}
else {
	webfonts.custom = {
		families: ['Font Awesome 5 Pro'],
		urls: [`${config.themeurl}/css/fonts.css`],
	}
}

WebFont.load(webfonts);
