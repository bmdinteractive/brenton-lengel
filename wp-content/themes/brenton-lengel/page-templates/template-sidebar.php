<?php
/**
 * Template Name: Page with Sidebar
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="content-col">

<?php
while ( have_posts() ) : the_post();
?>
			<?php get_template_part( 'loop-templates/content', 'page' ); ?>

<?php
endwhile;
?>

		</div><!-- .content-col -->

		<div class="sidebar-col">
			<div class="sidebar-sticky">

				<?php get_sidebar( 'page' ); ?>

			</div>
		</div><!-- .sidebar-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php
get_footer();
