<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package birdstrap
 */

if ( ! is_active_sidebar( 'blog-sidebar' ) ) {
	return;
}
?>

<div class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'blog-sidebar' ); ?>

</div><!-- .widget-area -->
