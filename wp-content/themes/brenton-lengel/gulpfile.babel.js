/**
 * Gulpfile
 *
 * Main tasks:
 * - gulp (dev)
 * - gulp build
 */

'use strict';

import gulp from 'gulp';
import plugins from 'gulp-load-plugins';
import mergeStream from 'merge-stream';
import mergeOptions from 'merge-options';
import stripComments from 'strip-comments';
import autoprefixer from 'autoprefixer';
import webpackStream from 'webpack-stream';
import webpack from 'webpack';
import named from 'vinyl-named';
import del from 'del';
import fs from 'fs';

const $ = plugins(); // Load all gulp-* plugins
const prompt = require('readline-sync');
const browserSync = require('browser-sync').create();
const BabelMinify = require('babel-minify-webpack-plugin');

const paths = {
	styleSrc: 'src/scss/',
	fontStyleSrc: 'src/scss/fonts.scss',
	styles: 'css/',
	scriptSrc: 'src/js/',
	scripts: 'js/',
	fonts: 'fonts/',
	// Only the JS files in the root of the /src/js/ folder will be processed by Webpack
	scriptEntries: ['src/js/*.js'],
	img: 'img/',
	node: 'node_modules/',
	modules: 'src/js/modules/',
	vendor: 'src/js/vendor/',
};


/**
 * Config
 */
let config = {}; // Filled by configLoad()
const defaultConfig = {
	devDomain: 'localhost',
	fontawesome: ['solid', 'regular', 'light', 'brands'],
	browsersync: {
		//proxy: config.devDomain,
		//host: config.devDomain,
		files: [
			//'./css/*.min.css', // handled via gulp task
			//'./js/*.min.js', // handled via gulp task
			'./img/*.{png,jpg,gif,svg}',
			'./**/*.php',
		],
		notify: true,
		open: 'external',
		snippetOptions: {
			ignorePaths: ['wp-admin/**'],
		},
	},
	webpack: {
		devtool: 'source-map',
		module: {
			rules: [
				{
					test: /.js$/,
					exclude: /node_modules/, // Comment out if you get errors from included modules
					use: {
						loader: 'babel-loader',
					},
				},
			],
		},
		resolve: {
			modules: [paths.modules, paths.vendor, 'node_modules'],
		},
		plugins: [
			new BabelMinify(),
		],
		externals: {
			jquery: 'jQuery',
		},
	},
};

let fontawesomeSassImports = []; // Filled by configLoad()


/**
 * == GULP TASKS ==
 */

/**
 * Run: gulp config
 *
 * Changes package name and copies sample config file if needed
 */
gulp.task('config',
	gulp.series(configCopy, configLoad));

/**
 * Run: gulp build
 *
 * Runs the build process that cleans & processes CSS & JS
 */
gulp.task('build',
	gulp.series(/*clean,*/ 'config', gulp.parallel(fonts, styles, scripts, version)));


/**
 * Run: gulp
 *
 * Runs build, then starts the BrowserSync server and watches files for auto-refresh
 */
gulp.task('default',
	gulp.series('build', server, watch));


/**
 * == FUNCTIONS ==
 */

/**
 * Clean destination folders
 *
 * Also cleans fontawesome font files, leaving other font files in place
 *
 * @deprecated 0.18.0 This is generally no longer needed, as each task cleans
 *                    its own destination folder
 */
function clean(done) {
	del([`${paths.styles}*`, `!${paths.styles}`, `!${paths.styles}/static`, `${paths.scripts}*`, `!${paths.scripts}`, `!${paths.scripts}/static`, `${paths.fonts}fa-*`]).then((paths) => {
		done();
	});
}

/**
 * Copy config-sample.json to config.json
 */
function configCopy(done) {
	if (fs.existsSync('./config.json')) {
		console.log('File config.json already exists');
		done();
		return;
	}

	const name = prompt.question('Input the theme name (lowercase only):', {defaultInput: false});

	const packageStream = gulp.src('./package.json');
	if (name) {
		packageStream.pipe($.replace(/"name": "birdstrap"/m, `"name": "${name}"`))
		.pipe($.replace(/"version": "[^"]*?"/m, `"version": "1.0.0"`))
		.pipe(gulp.dest('./'));
	}

	const domain = prompt.question('Input your local development domain (ex: clientdomain.local, default: localhost):', {defaultInput: 'localhost'});

	const configStream = gulp.src('config-sample.json')
	.pipe($.replace(/"devDomain": "localhost"/m, `"devDomain": "${domain}"`))
	.pipe($.rename('config.json'))
	.pipe(gulp.dest('./'));

	return mergeStream(packageStream, configStream);
}

function configLoad(done) {
	let devConfig = {};
	if ( fs.existsSync('config.json') ) {
		try {
			devConfig = JSON.parse(stripComments(fs.readFileSync('config.json', { encoding: 'utf-8' })));
		}
		catch(e) {
			console.error(e);
		}
	}
	let themeConfig = {};
	if ( fs.existsSync('themeconfig.json') ) {
		try {
			themeConfig = JSON.parse(stripComments(fs.readFileSync('themeConfig.json', { encoding: 'utf-8' })));
		}
		catch(e) {
			console.error(e);
		}
	}
	// Set global config object
	config = mergeOptions(defaultConfig, themeConfig, devConfig);

	// Handle devDomain for BrowserSync
	if (config.devDomain) {
		if (!config.browsersync.proxy) {
			config.browsersync.proxy = config.devDomain;
		}
		if (!config.browsersync.host) {
			config.browsersync.host = config.devDomain;
		}
	}

	// Font Awesome font file paths
	const allowedFAStyles = ['solid', 'regular', 'light', 'brands'];
	if (!config.hasOwnProperty('fontawesome') || config.fontawesome.length === 0) {
		// All styles available by default
		config.fontawesome = allowedFAStyles;
	}
	paths.fontawesome = [];
	fontawesomeSassImports = [
		'$fa-font-path: "../fonts";',
		'@import "@fortawesome/fontawesome-pro/scss/fontawesome";',
	];
	for (const style of config.fontawesome) {
		if (allowedFAStyles.includes(style)) {
			paths.fontawesome.push(`./node_modules/@fortawesome/fontawesome-pro/webfonts/fa-${style}-*`);
			fontawesomeSassImports.push(`@import "@fortawesome/fontawesome-pro/scss/${style}";`);
		}
	}
	done();
}

/**
 * Moves fonts into the theme
 */
function fonts() {
	console.log('Copying fonts:', paths.fontawesome);
	return gulp.src(paths.fontawesome)
	.pipe(gulp.dest(paths.fonts))
	// Clean extraneous files
	.pipe($.destClean(paths.fonts, 'static/**'));
}

/**
 * Compiles & compresses Sass to CSS (minified)
 */
function styles() {
	const stream = mergeStream(
		// We handle fonts.scss separately to inject the specified Font Awesome styles
		gulp.src(paths.fontStyleSrc, { sourcemaps: true })
		.pipe($.injectString.prepend(fontawesomeSassImports.join('\n'))),
		// Add the rest of the sass files
		gulp.src(paths.styleSrc + '*.scss', { ignore: paths.fontStyleSrc, sourcemaps: true })
	)
	.pipe($.plumber({
		errorHandler: function (err) {
			console.error(err);
			this.emit('end');
		}
	}))
	// Initial Sass compilation
	.pipe($.sassGlob())
	.pipe($.sass({
		includePaths: ['node_modules'],
		errLogToConsole: true,
		outputStype: 'expanded', // expanded, nested, compact, compressed
	}))
	// PostCSS: Autoprefixer, minify
	.pipe($.postcss([
		autoprefixer(),
	]))
	.pipe($.cleanCss({
		level: 2,
	}))

	// Ouput CSS and maps
	.pipe(gulp.dest(paths.styles, { sourcemaps: '.' }))
	// Clean extraneous files
	.pipe($.destClean(paths.styles, 'static/**'));

	if (browserSync.active) {
		stream.pipe(browserSync.stream());
	}

	return stream;
}

/**
 * Webpack script files from paths.scriptEntries files
 */
function scripts() {
	const stream = gulp.src(paths.scriptEntries, { sourcemaps: true })
	.pipe($.plumber({
		errorHandler: function (err) {
			console.error(err);
			this.emit('end');
		}
	}))
	.pipe(named())
	// We have to clone the config to deal with new/renamed files
	.pipe(webpackStream(Object.assign({}, config.webpack), webpack))
	.pipe(gulp.dest(paths.scripts, { sourcemaps: '.' }))
	// Clean extraneous files
	.pipe($.destClean(paths.scripts, 'static/**'));

	if (browserSync.active) {
		stream.pipe(browserSync.stream());
	}

	return stream;
}

/**
 * Updates theme version number in style.css
 */
function version() {
	const json = JSON.parse(fs.readFileSync('package.json'));
	console.log('Copying version from package.json to style.css: '+json.version);

	const stream = gulp.src('style.css');

	// Add Birdstrap Version to CSS
	if (json.name === 'birdstrap') {
		stream.pipe($.replace(/^Birdstrap Version: \d+\.\d+\.\d+$/m, 'Birdstrap Version: ' + json.version));
	}
	// Add theme version to CSS
	else {
		stream.pipe($.replace(/^Version: \d+\.\d+\.\d+(.+?)?$/m, 'Version: ' + json.version));
	}

	stream.pipe(gulp.dest('./'));

	return stream;
}

/**
 * Start BrowserSync server which watches files defined in browsersync.files
 */
function server(done) {
	browserSync.init(config.browsersync);
	done();
}

/**
 * Reload the browser with BrowserSync
 */
function reload(done) {
	browserSync.reload();
	done();
}

function watch() {
	gulp.watch(paths.styleSrc + '**/*.scss', {cwd: './'}, gulp.series(styles));
	gulp.watch(paths.scriptSrc + '**/*.js', {cwd: './'}, gulp.series(scripts));
	gulp.watch('package.json', {cwd: './'}, gulp.series(version));
	gulp.watch(['themeconfig.json', 'config.json'], { cwd: './'}, gulp.series('build'));

	// PHP & image files are watched directly by BrowserSync
	//gulp.watch('**/*.php').on('all', browserSync.reload);
	//gulp.watch('src/assets/img/**/*').on('all', gulp.series(images, browserSync.reload));
}
