<?php
/**
 * Component: link button
 */

$url = get_required_prop('url');
$target = get_prop('target', '');
$title = get_prop('title', 'Read More');
$class = get_prop('class', 'btn-primary');
?>

<a href="<?php echo esc_url($url); ?>" class="btn <?php esc_attr_e($class); ?>" target="<?php esc_attr_e($target); ?>"><?php echo esc_html_e($title); ?></a>
