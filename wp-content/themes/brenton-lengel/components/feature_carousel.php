<?php
/**
 * Component: Feature Carousel
*/
global $post;

$slides = get_required_prop('slides');

// print_r($props);
$alignment = 'align'.get_prop('align');

$interval = 800000;

$block_id = get_prop('id');


?>
<div class="wp-block-feature-carousel carousel <?php echo $alignment; ?>">
	<div id="<?php echo $block_id ?>" class="carousel slide" data-ride="carousel" data-interval="<?php echo $interval; ?>">
	<?php  
	$slide_count = 0;

	while(have_rows('slides')): the_row();
		$item_class = ($slide_count==0) ? 'active ' : '';

		$slide_type = get_sub_field('type');
		$slide_description = null;
		if($slide_type=='custom'):
			$slide_headline = get_sub_field('headline');
			$slide_description = get_sub_field('description');
			$slide_image = get_sub_field('image');
			$slide_image = $slide_image['id'];
			$item_link = get_sub_field('link');
			$item_url = $item_link['url'];
			$item_link_target = $item_link['target'];
			$link_label = (!empty($item_link['title'])) ? $item_link['title'] : "Learn More";
		else :
			$slide_posts = get_sub_field('slide_data');
			foreach($slide_posts as $post):
				setup_postdata( $post );
				$slide_headline = get_the_title($post);
				$slide_image = get_post_thumbnail_id( $post );
				$slide_description = get_the_excerpt();
				$item_url = get_permalink( $post);
				$item_link_target = "";
				$link_label = "Learn More";
			endforeach;
		endif;


		?>

		<div class="carousel-item <?php echo $item_class; ?>">
			<div class="gradient-overlay"></div>
			<?php 
			echo get_lazy_attachment_image( $slide_image, 'large', false, array( 'class' => 'background_img' ) ); 
			?>

			<div class="container carousel-item-container">
				<div class="row">
					<div class="carousel-item-content">
						<h3><a href="<?php echo $item_url; ?>" target="<?php echo $item_link_target; ?>"><?php echo $slide_headline; ?></a></h3>
						<?php if($slide_description): ?>
						<div class="slide-description">
							<?php echo $slide_description; ?>
						</div>
						<?php endif; ?>
						<div class="slide-cta">
							<a href="<?php echo $item_url; ?>" class="btn" target="<?php echo $item_link_target; ?>"><?php echo $link_label; ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
		$slide_count++;
	endwhile;
	?>
		<div class="carousel-indicators-container container-fluid">
			<ol class="carousel-indicators">
				<?php 
				$total_slides = $slide_count;
				$slide_count=0; 
				while($slide_count < $total_slides):
					$item_class = ($slide_count==0) ? 'active ' : '';
				?>
			    <li data-target="#<?php echo $block_id; ?>" data-slide-to="<?php echo $slide_count; ?>" class="<?php echo $item_class; ?>"></li>
				<?php 
					$slide_count++;
				endwhile; ?>
			  </ol>
		</div>		
	</div>		
	<div class="carousel-controls">
		<a class="carousel-control-prev" href="#<?php echo $block_id; ?>" role="button" data-slide="prev">
			<i class="fas fa-angle-left"></i>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#<?php echo $block_id; ?>" role="button" data-slide="next">
			<i class="fas fa-angle-right"></i>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
