<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package birdstrap
 */

get_header();

$curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug', $author_name ) : get_userdata( intval( $author ) );
?>

<div class="container">
	<div class="row">

		<div class="col content-col">

			<header class="page-header page-header--author">

				<h1 class="site-title"><?php esc_html_e( 'About:', 'birdstrap' ); ?><?php echo esc_html( $curauth->nickname ); ?></h1>

<?php
if ( ! empty( $curauth->ID ) ) :
?>
				<?php echo get_avatar( $curauth->ID ); ?>
<?php
endif;
?>

				<dl>
<?php
if ( ! empty( $curauth->user_url ) ) :
?>
					<dt><?php esc_html_e( 'Website', 'birdstrap' ); ?></dt>
					<dd>
						<a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo esc_html( $curauth->user_url ); ?></a>
					</dd>
<?php
endif;

if ( ! empty( $curauth->user_description ) ) :
?>
					<dt><?php esc_html_e( 'Profile', 'birdstrap' ); ?></dt>
					<dd><?php echo esc_html( $curauth->user_description ); ?></dd>
<?php
endif;
?>
				</dl>

				<h2><?php esc_html_e( 'Posts by', 'birdstrap' ); ?> <?php echo esc_html( $curauth->nickname ); ?>:</h2>

			</header><!-- .page-header -->

<?php
if ( have_posts() ) :
?>

			<ul class="author-posts">
<?php
	while ( have_posts() ) : the_post();
?>
				<li>
					<a rel="bookmark" href="<?php the_permalink() ?>" title="<?php esc_html_e( 'Permanent Link:', 'birdstrap' ); ?> <?php the_title(); ?>">
						<?php the_title(); ?></a>,
					<?php birdstrap_posted_on(); ?> <?php esc_html_e( 'in',
					'birdstrap' ); ?> <?php the_category( '&' ); ?>
				</li>
<?php
	endwhile;
?>
			</ul>

<?php
else :
?>

			<?php get_template_part( 'loop-templates/none' ); ?>

<?php
endif;
?>

		</div><!-- .content-col -->

		<div class="col-md-<?php sidebar_columns(); ?> <?php sidebar_position(); ?> sidebar-col">

			<?php get_sidebar( 'blog' ); ?>

		</div><!-- .sidebar-col -->

	</div><!-- .row -->

	<?php birdstrap_pagination(); ?>

</div><!-- .container -->

<?php
get_footer();
