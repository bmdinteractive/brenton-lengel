<?php
/**
 * Default content template part
 *
 * @package birdstrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>

<?php
if ( 'post' === get_post_type() ) :
?>

		<div class="entry-meta">

			<?php birdstrap_posted_on(); ?>

		</div><!-- .entry-meta -->

<?php
endif;
?>

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<?php birdstrap_pagination(); ?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php birdstrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
