<?php
/**
 * Partial template for content in page.php
 *
 * @package birdstrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php 

	/*
	** Feature Image
	*/
	if(has_post_thumbnail()):  
	?>
	<header class="entry-header has-img">
		<div class="feature-image">
			<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
		</div>
		
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</header><!-- .entry-header -->
	<?php else : ?>

	<header class="entry-header">
		
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</header><!-- .entry-header -->


	<?php endif; ?>

	

	<div class="entry-content">

		<?php the_content(); ?>

		<?php birdstrap_pagination(); ?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
