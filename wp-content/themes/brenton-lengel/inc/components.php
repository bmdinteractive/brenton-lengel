<?php
/**
 * Component functions
 *
 * Components are like an extension of the get_template_part() functionality
 * built into WordPress. They must exist in the root of the components folder.
 * Use the get_prop() function to get values from the arrays passed in.
 * You can pass in as many arrays as you want—they will all be merged into a
 * single $props array. Successive arrays with the same keys will overwrite
 * earlier arrays.
 *
 * This system is great for components that can be used directly and also with
 * ACF flexible content fields.
 */


/**
 * Output template from components folder
 *
 * @todo refactor this system to be class-based
 */
function the_component( $component = false, $props = array(), ...$extras ) {
	if ( $component === false ) return;

	if ( ! is_array( $props ) ) {
		$props = array();
	}

	if ( $extras ) {
		trigger_error('The $extras argument to ' . __METHOD__ . ' is deprecated. Use array_merge to supply a single $props array instead.', E_USER_DEPRECATED);
		foreach ( $extras as $extra ) {
			if ( is_array( $extra ) ) {
				$props = array_merge( $props, $extra );
			}
		}
	}

	// Set $props as globals for the get_prop() util
	global $_props;
	$last_props = $_props;
	$_props = $props;

	$inc = locate_template( array( 'components/' . $component . '.php' ) );
	if ( ! empty( $inc ) ) {
		try {
			include $inc;
		}
		catch ( ComponentException $e ) {
			if ( WP_DEBUG ) error_log($e);
		}
	}

	// Reset global vars (nested components anyone?)
	$_props = $last_props;
}
function get_component( $component = false, $props = array() ) {
	ob_start();
	the_component( $component, $props );
	return ob_get_clean();
}


/**
 * Utility functions for components
 */

/**
 * Get the $props array
 *
 * @since 0.14.0
 * @return array|boolean Props array or false
 */
function get_props() {
	global $_props;

	return $_props ?: false;
}

/**
 * Get single prop from the $props array
 *
 * @since 0.14.0
 * @param string $key Key of the prop to retrieve
 * @param mixed $default Value to return if key is not found
 * @return mixed Prop value
 */
function get_prop( $key, $default = false ) {
	global $_props;

	if ( is_array( $_props ) && ( isset( $_props[$key] ) || array_key_exists( $key, $_props ) ) ) {
		return $_props[$key];
	}

	return $default;
}


/**
 * Get a single required prop from the $props array.
 *
 * If not found, the component will not render.
 *
 * @since 0.15.0
 * @param string $name Name of the required prop to retrieve
 * @return mixed Prop value
 */
function get_required_prop( $name ) {
	global $_props;

	if ( is_array( $_props ) && ( isset( $_props[$name] ) || array_key_exists( $name, $_props ) ) ) {
		return $_props[$name];
	}

	throw new ComponentException('Required prop not found: '.$name);
}

/**
 * Get a single prop with an expected type from the $props array.
 *
 * @since 0.15.0
 * @param string $name Name of the prop to retrieve
 * @param string $type gettype-compatible type string
 * @param mixed $default Value to return if prop is not found or is not typed correctly
 * @return mixed Prop value
 */
function get_typed_prop( $name, $type, $default = false ) {
	$prop = get_required_prop( $name );

	if ( gettype( $prop ) === $type ) {
		return $prop;
	}

	return $default;
}

/**
 * Get a single prop with an expected value from the $props array.
 *
 * @since 0.15.0
 * @param string $name Name of the required prop to retrieve
 * @param array $enum Array of expected values (strictly type-checked)
 * @param mixed $default Value to return if prop is not found or is not within expected values
 * @return mixed Prop value
 */
function get_enum_prop( $name, $enum, $default = false ) {
	$prop = get_required_prop( $name );

	if ( in_array( $prop, $enum, true ) ) {
		return $prop;
	}

	return $default;
}

if ( ! class_exists( 'ComponentException' ) ) :
class ComponentException extends Exception { }
endif;


/**
 * DEPRECATED: Returns $props array. Originally retreived the separate $extras
 * array, which has since been merged into $props
 *
 * @deprecated
 *
 * @since 0.14.0
 * @return array|boolean Extras array or false
 */
function get_extras() {
	trigger_error('Method ' . __METHOD__ . ' is deprecated. Use get_props() instead.', E_USER_DEPRECATED);

	return get_props();
}

/**
 * DEPRECATED: Originally retreived a single extra from the separate $extras
 * array, which has since been merged into a single $props array
 *
 * @deprecated
 *
 * @since 0.14.0
 * @param string $key Key of the extra to retrieve
 * @param mixed $default Value to return if key is not found
 * @return mixed Extra value
 */
function get_extra( $key, $default = false ) {
	trigger_error('Method ' . __METHOD__ . ' is deprecated. Use get_prop() instead.', E_USER_DEPRECATED);

	return get_prop( $key, $default );
}
