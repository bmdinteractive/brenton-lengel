<?php
/**
 * Page Navigation widget
 *
 * @package birdstrap
 */


class Pagenav_Widget extends WP_Widget {

	public $args = array(
		'before_widget' => '<div class="widget-wrap widget_pagenav">',
		'after_widget' => '</div>'
	);

	function __construct() {

		parent::__construct(
			'birdstrap-pagenav',
			'Page Navigation Widget',
			array(
				'description' => __( 'Display subpage navigation automatically based on the page hierarchy', 'birdstrap' )
			)
		);

		add_action( 'widgets_init', function() {
			register_widget( 'Pagenav_Widget' );
		} );
	}

	public function widget( $args, $instance ) {
		global $post;

		if ( ! isset( $post ) || get_post_type( $post ) !== 'page' ) return;

		$depth = empty( $args['depth'] ) ? 0 : $args['depth'];

		$is_parent = true;
		$main_id = $post->ID;
		$ancestors = get_post_ancestors( $post->ID );

		if ( $ancestors ) {
			$is_parent = false;
			$main_id = $ancestors[count($ancestors) - 1];
		}

		$children = wp_list_pages( array(
			'sort_column' => 'menu_order',
			'depth' => $depth,
			'title_li' => null,
			'child_of' => $main_id,
			'echo' => false
		) );

		if ( $children ) :
			echo $args['before_widget'];
?>
<nav class="pagenav">
<?php
			if ( $is_parent ) :
?>
	<h3><?php echo esc_html( get_the_title( $main_id ) ); ?></h3>
<?php
			else:
?>
	<h3><a class="parent-menu-link" href="<?php echo esc_url( get_page_link( $main_id ) ); ?>"><?php echo esc_html( get_the_title( $main_id ) ); ?></a></h3>
<?php
			endif;
?>
	<ul class="menu sidebar-menu">
		<?php echo $children; ?>
	</ul>
</nav>
<?php
			echo $args['after_widget'];
		endif;

	}

	public function form( $instance ) {
		$depth = empty( $instance['depth'] ) ? 0 : intval( $instance['depth'] );
?>
<p>
	<label for="<?php echo esc_attr( $this->get_field_id( 'depth' ) ); ?>"><?php esc_attr_e( 'Depth:', 'birdstrap' ); ?></label>
	<input class="" id="<?php echo esc_attr( $this->get_field_id( 'depth' ) ); ?>" name="<?php echo esc_attr( $this->get_field_id( 'depth' ) ); ?>" type="number" value="<?php echo esc_attr( $depth ); ?>" /><br />
	<small>How many levels of nested pages to show (0 = all)</small>
</p>
<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['depth'] = empty( $new_instance['depth'] ) ? 0 : intval( $new_instance['depth'] );

		return $instance;
	}
}
$birdstrap_pagenav = new Pagenav_Widget();
