<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Functions that rely on context should go in template-tags.php
 *
 * @package birdstrap
 */

/**
 * Get image tag for use with Lazysizes JS
 */
function get_lazy_attachment_image( $attachment_id, $size = 'preview', $icon = false, $attr = '' ) {
	$default_attr = array( 'class' => 'lazyload blur-up' );
	$attr = wp_parse_args( $default_attr, $attr );
	$img = wp_get_attachment_image( $attachment_id, $size, $icon, $attr );
	$lq_src = wp_get_attachment_image_src( $attachment_id, 'preview' );
	$lazy_img = str_replace( array( ' src=', ' srcset=', '<img' ), array( ' data-src=', ' data-srcset=', '<img src="'.$lq_src[0].'"' ), $img );

	return '<noscript>'.$img.'</noscript>'.$lazy_img;
}
function social_links() {
	if ( ! have_rows( 'social_links', 'options' ) ) return;

?>
<ul class="social-link-list">
<?php
	while ( have_rows( 'social_links', 'options' ) ) : the_row();
		$social = get_sub_field( 'icon' );
		if ( $social['value'] == 'custom' ) {
			$social = array(
				'value' => 'fa-'.get_sub_field('custom_icon'),
				'label' => get_sub_field('custom_label'),
			);
		}
?>
	<li><a href="<?php echo esc_url( get_sub_field( 'url' ) ); ?>" target="_blank" rel="nofollow noopener" aria-label="<?php echo esc_attr($social['label']); ?>"><i class="fab <?php echo esc_attr($social['value']); ?>" aria-hidden="true" title="<?php echo esc_attr($social['label']); ?>"></i></a></li>
<?php
	endwhile;
?>
</ul>
<?php
}