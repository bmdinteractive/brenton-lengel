<?php
/**
 * Birdstrap modify editor
 *
 * @package birdstrap
 */

if ( ! function_exists( 'birdstrap_enqueue_block_editor_assets' ) ) :
/**
 * Enqueue scripts/styles for the backend Gutenberg editor
 */
function birdstrap_enqueue_block_editor_assets() {

	/**
	 * Enqueue block editor JS
	 */
	wp_enqueue_script( 'birdstrap-blocks', get_stylesheet_directory_uri() . '/js/blocks.js', array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-dom-ready', 'wp-edit-post', 'wp-i18n' ), cache_buster( '/js/blocks.js' ) );

	// Webfonts
	// TODO: This causes an error in the editor, WTF
	//birdstrap_enqueue_fonts();

	/**
	 * Enqueue block editor styles
	 *
	 * NOTE: currently editor.css is included by `birdstrap_add_editor_styles`
	 * which is applied within the editor. If you need to apply styles to the
	 * editor UI (sidebar panels, etc) enqueue the stylesheet here.
	 */
	//wp_enqueue_style( 'birdstrap-blocks', get_stylesheet_directory_uri() . '/css/block-editor.css', array( 'wp-blocks' ), cache_buster( '/css/block-editor.css' ) );
}
add_action( 'enqueue_block_editor_assets', 'birdstrap_enqueue_block_editor_assets' );
endif;


/**
 * Registers an editor stylesheet for the theme.
 */
function birdstrap_add_editor_styles() {
	add_theme_support( 'editor-styles' );
	add_editor_style( 'css/editor.css' );
}
add_action( 'after_setup_theme', 'birdstrap_add_editor_styles' );

/**
 * Add TinyMCE style formats.
 */
function birdstrap_tiny_mce_style_formats( $styles ) {
	array_unshift( $styles, 'styleselect' );
	return $styles;
}
add_filter( 'mce_buttons_2', 'birdstrap_tiny_mce_style_formats' );

function birdstrap_tiny_mce_before_init( $settings ) {

	$style_formats = array(
		array(
			'title' => 'Lead Paragraph',
			'selector' => 'p',
			'classes' => 'lead',
			'wrapper' => true
		),
		array(
			'title' => 'Button Link',
			'selector' => 'a',
			'classes' => 'btn',
			'wrapper' => false
		),
		array(
			'title' => 'Small',
			'inline' => 'small'
		),
		array(
			'title' => 'Blockquote',
			'block' => 'blockquote',
			'classes' => 'blockquote',
			'wrapper' => true
		),
		array(
			'title' => 'Blockquote Footer',
			'block' => 'footer',
			'classes' => 'blockquote-footer',
			'wrapper' => true
		),
		/*
		array(
			'title' => 'Code Block',
			'block' => 'code',
			'wrapper' => true
		),
		array(
			'title' => 'Cite',
			'inline' => 'cite'
		)
		*/
	);

	if ( isset( $settings['style_formats'] ) ) {
		$orig_style_formats = json_decode($settings['style_formats'],true);
		$style_formats = array_merge($orig_style_formats,$style_formats);
	}

	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
}
add_filter( 'tiny_mce_before_init', 'birdstrap_tiny_mce_before_init' );
