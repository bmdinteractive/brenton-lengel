<?php
/**
 * Filters & Actions for native WordPress functionality
 */

if ( ! function_exists( 'birdstrap_mime_types' ) ) :
/**
 * Enable media upload mime types
 */
function birdstrap_mime_types( $mimes ) {
	// Enable SVG
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
endif;
add_filter( 'upload_mimes', 'birdstrap_mime_types' );


if ( ! function_exists( 'birdstrap_custom_excerpt_more' ) ) :
/**
 * Removes the ... from the excerpt read more link
 *
 * @param string $more The excerpt.
 *
 * @return string
 */
function birdstrap_custom_excerpt_more( $more ) {
	return '';
}
endif;
add_filter( 'excerpt_more', 'birdstrap_custom_excerpt_more' );


if ( ! function_exists( 'birdstrap_all_excerpts_get_more_link' ) ) :
/**
 * Adds a custom read more link to all excerpts, manually or automatically generated
 *
 * @param string $post_excerpt Posts's excerpt.
 *
 * @return string
 */
function birdstrap_all_excerpts_get_more_link( $post_excerpt ) {
	return $post_excerpt . '<br /><a class="btn btn-secondary birdstrap-read-more-link" href="' . esc_url( get_permalink( get_the_ID() )) . '">' . __( 'Read More',
	'birdstrap' ) . ' <i class="fa fa-arrow-right"></i></a>';
}
endif;
//add_filter( 'wp_trim_excerpt', 'birdstrap_all_excerpts_get_more_link' );


/**
 * Lowercase all file uploads so there are no filename collisions on case-insensitive filesystems
 */
function birdstrap_filename_lowercase( $filename ) {
	return strtolower( $filename );
}
add_filter( 'sanitize_file_name', 'birdstrap_filename_lowercase' );

if ( ! function_exists( 'birdstrap_body_classes' ) ) :
/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function birdstrap_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
endif;
add_filter( 'body_class', 'birdstrap_body_classes' );


if ( ! function_exists( 'birdstrap_adjust_body_class' ) ) :
/**
 * Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
 *
 * @param string $classes CSS classes.
 *
 * @return mixed
 */
function birdstrap_adjust_body_class( $classes ) {

	foreach ( $classes as $key => $value ) {
		if ( 'tag' == $value ) {
			unset( $classes[ $key ] );
		}
	}

	return $classes;

}
endif;
add_filter( 'body_class', 'birdstrap_adjust_body_class' );


/**
 * Humans.txt
 */
function birdstrap_render_humans_txt() {
	if ( $_SERVER['REQUEST_URI'] == '/humans.txt' ) {
		header( 'Content-Type: text/plain; charset=utf-8' );
		include get_template_directory() . '/humans.txt';
		exit();
	}
}
add_action( 'parse_request', 'birdstrap_render_humans_txt' );
function birdstrap_link_humans_txt() {
	echo '<link rel="author" href="'.esc_url( home_url( 'humans.txt' ) ).'" />';
}
add_action( 'wp_head', 'birdstrap_link_humans_txt' );
