<?php
/**
 * Pagination functions
 *
 * @package birdstrap
 */


/**
 * Filters wp_link_pages to include Bootstrap markup
 *
 * @since 0.17.0
 */
function birdstrap_link_pages_link( $link ) {
	$active = strpos( $link, 'current' ) !== false ? 'active' : '';
	return '<li class="page-item ' . $active . '">' . str_replace( 'class="post-page-numbers', 'class="page-link', $link ) . '</li>';
};

add_filter( 'wp_link_pages_link', 'birdstrap_link_pages_link' );


if ( ! function_exists( 'birdstrap_pagination' ) ) :
/**
 * Paginate all the things!
 *
 * Handles page breaks, adjacent posts, and archive pagination.
 *
 * @since 0.17.0
 */
function birdstrap_pagination() {
	if ( is_singular() ) {
		// Show nav for post with page breaks
		wp_link_pages( array(
			'before' => '<nav class="navigation pagination-nav" role="navigation" aria-label="Page navigation"><h6 class="d-inline">'.__( 'Pages:', 'birdstrap' ).'</h6> <ul class="pagination d-inline-flex">',
			'after'  => '</ul></nav>',
		) );
	}
	else {
		// Pagination for post archives
		// NOTE: I wanted to use get_the_posts_pagination but it's too difficult
		// currently to override classes/structure
		$links = paginate_links( array(
			'type' => 'array',
			'prev_text' => '<i class="fa fa-angle-left" aria-label="Previous"></i>',
			'next_text' => '<i class="fa fa-angle-right" aria-label="Next"></i>',
		) );

		if(is_array($links)):
			$links = array_map( function($l) {
				$active = '';
				if ( strpos( $l, 'current' ) !== false ) {
					$active = 'active';
				}
				// Replace link class
				$l = str_replace( 'page-numbers', 'page-link', $l );
				return sprintf( '<li class="page-item %s">%s</li>', $active, $l );
			}, $links );
?>
<nav class="navigation pagination-nav" role="navigation" aria-label="Page navigation">
	<h6 class="screen-reader-text"><?php _e( 'Pages:', 'birdstrap' ); ?></h6>
	<ul class="pagination">
		<?php echo join( '', $links ); ?>
	</ul>
</nav>
<?php
		endif;
	}
}
endif;


if ( ! function_exists( 'birdstrap_comment_pagination' ) ) :
/**
 * Paginate comments
 *
 * @since 0.17.0
 */
function birdstrap_comment_pagination() {
		// Pagination for comments
		// NOTE: I wanted to use paginate_comments_links but it's too difficult
		// currently to override classes/structure
		if ( ! is_singular() ) {
			return;
		}

		$page = get_query_var( 'cpage' );
		if ( ! $page ) {
			$page = 1;
		}
		$max_page = get_comment_pages_count();
		$args = array(
			'base' => add_query_arg( 'cpage', '%#%' ),
			'format' => '',
			'total' => $max_page,
			'current' => $page,
			'add_fragment' => '#comments',
			'type' => 'array',
			'prev_text' => '<i class="fa fa-angle-left" aria-label="Previous"></i>',
			'next_text' => '<i class="fa fa-angle-right" aria-label="Next"></i>',
		);
		global $wp_rewrite;
		if ( $wp_rewrite->using_permalinks() ) {
			$args['base'] = user_trailingslashit( trailingslashit( get_permalink() ) . $wp_rewrite->comments_pagination_base . '-%#%', 'commentpaged' );
		}

		$links = paginate_links( $args );
		$links = array_map( function($l) {
			$active = '';
			if ( strpos( $l, 'current' ) !== false ) {
				$active = 'active';
			}
			// Replace link class
			$l = str_replace( 'page-numbers', 'page-link', $l );
			return sprintf( '<li class="page-item %s">%s</li>', $active, $l );
		}, $links );
?>
<nav class="navigation comments-nav" role="navigation" aria-label="Comment navigation">
	<h6 class="d-inline"><?php _e( 'Pages:', 'birdstrap' ); ?></h6>
	<ul class="pagination d-inline-flex">
		<?php echo join( '', $links ); ?>
	</ul>
</nav>
<?php
}
endif;


if ( ! function_exists( 'birdstrap_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 */
function birdstrap_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
?>
<nav class="navigation posts-nav" role="navigation" aria-label="Post navigation">
	<h6 class="screen-reader-text"><?php _e( 'Post navigation', 'birdstrap' ); ?></h6>
	<div class="row nav-links justify-content-between">
		<?php

			if ( get_previous_post_link() ) {
				previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'birdstrap' ) );
			}
			if ( get_next_post_link() ) {
				next_post_link( '<span class="nav-next">%link</span>', _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'birdstrap' ) );
			}
		?>
	</div><!-- .nav-links -->
</nav><!-- .post-navigation -->
<?php
}
endif;

if ( ! function_exists( 'birdstrap_page_nav' ) ) :
/**
 * Display navigation to next/previous page if applicable.
 */
function birdstrap_page_nav() {
	global $post;

	$is_parent = true;
	$main_id = $post->ID;
	$ancestors = get_post_ancestors( $post->ID );

	if ( $ancestors ) {
		$is_parent = false;
		$main_id = $ancestors[count($ancestors) - 1];
	}

	$children = get_pages( array(
		'sort_column' => 'menu_order',
		'sort_order' => 'DESC',
		'child_of' => $main_id,
	) );

	if ( count( $children ) > 0 ) {

		$child_ids = wp_list_pluck( $children, 'ID' );

		$current_index = false;
		$prev_index = false;
		$next_index = false;

		if ( $post->ID !== $main_id ) {
			$current_index = array_search( $post->ID, $child_ids );
			$prev_index = $current_index > 0 ? $current_index - 1 : false;
			$next_index = $current_index < ( count( $child_ids ) - 1 ) ? $current_index + 1 : false;
		}
		else {
			$next_index = 0;
		}

		if ( $current_index === 0 ) {
			$prev = get_post( $main_id );
		}
		else {
			$prev = $prev_index !== false ? $children[$prev_index] : false;
		}

		$next = $next_index !== false ? $children[$next_index] : false;

		if ( ! $prev && ! $next ) {
			return;
		}
?>
<nav class="navigation pages-nav" role="navigation" aria-label="Page navigation">
	<h6 class="screen-reader-text"><?php _e( 'Page navigation', 'birdstrap' ); ?></h6>
	<div class="row nav-links justify-content-between">
<?php
		if ( $prev ) :
			$prev_title = $prev->post_title;
			if ( empty( $prev_title ) ) $prev_title = __('Previous page', 'birdstrap' );
			$prev_title = apply_filters( 'the_title', $prev_title, $prev->ID );
?>
		<a class="btn btn-primary nav-prev" href="<?php echo get_permalink( $prev ); ?>"><i class="fa fa-chevron-left"></i> <?php echo $prev_title; ?></a>
<?php
		else :
			echo '<div class="spacer"></div>';
		endif;
		if ( $next ) :
			$next_title = $next->post_title;
			if ( empty( $prev_title ) ) $prev_title = __('Next page', 'birdstrap' );
		$next_title = apply_filters( 'the_title', $next_title, $next->ID );
?>
		<a class="btn btn-primary nav-next" href="<?php echo get_permalink( $next ); ?>"><?php echo $next_title; ?> <i class="fa fa-chevron-right"></i> </a>
<?php
		endif;
?>
	</div><!-- .nav-links -->
</nav><!-- .post-navigation -->
<?php
	}
}
endif;
