<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package birdstrap
 */

get_header();
?>

<div class="container">
	<div class="row">

		<div class="col content-col">

<?php
if ( have_posts() ) :
?>

			<header class="page-header page-header--archive page-header--index">
				<?php the_archive_title( '<h1 class="site-title">', '</h1>' ); ?>
				<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
			</header><!-- .page-header -->

			<div class="archive-list archive-list--index">

<?php
	while ( have_posts() ) : the_post();
		// Support post formats, otherwise look for custom post type tempalate
		$type = get_post_type();
		if ( $type == 'post' ) {
			$type = get_post_format();
		}
?>

				<?php get_template_part( 'loop-templates/excerpt', $type ); ?>

<?php
	endwhile;
?>
			</div><!-- .archive-list -->

			<?php birdstrap_pagination(); ?>
<?php
else :
?>

			<?php get_template_part( 'loop-templates/none' ); ?>

<?php
endif;
?>

		</div><!-- .content-col -->

		<div class="col-md-<?php sidebar_columns(); ?> <?php sidebar_position(); ?> sidebar-col">

			<?php get_sidebar( 'blog' ); ?>

		</div><!-- .sidebar-col -->

	</div><!-- .row -->
</div><!-- .container -->

<?php
get_footer();
